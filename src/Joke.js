import React, { Component } from 'react';
import './Joke.css';

const colors = [
    'red',
    'orange',
    'grey',
    'greenyellow',
    'lime',
    'limegreen',
    'green'
];


class Joke extends Component {

    constructor(props){
        super(props);

        
        this.handleIncPoints = this.handleIncPoints.bind(this);
        this.handleDecPoints = this.handleDecPoints.bind(this);
    }

    handleIncPoints(){
        this.props.handlePoints('inc', this.props.id);
    }

    handleDecPoints(){
        this.props.handlePoints('dec', this.props.id);
    }

    pickColor(points){
        let color;
        if(points <= -10){
            color = colors[0];
        }else if(points > -10 && points < 0){
            color = colors[1];
        }else if(points === 0){
            color = colors[2];
        }else if(points > 0 && points <= 3){
            color = colors[3];
        }else if(points > 3 && points <= 6){
            color = colors[4];
        }else if(points > 6 && points <= 9){
            color = colors[5];
        }else if(points > 9){
            color = colors[6];
        }

        return color;
    }

    render(){

        const color = this.pickColor(this.props.points);
    

        return(
            <li className={this.props.focus ? 'Joke Joke-Focus' : 'Joke'}>
                <div className='Joke-Points'>
                    <i className="fas fa-arrow-up Joke-PointsButtons" onClick={this.handleIncPoints}></i>
                    <div className="Joke-Votes Joke-Shadow" style={{border: `3px solid ${color}`}}>
                        <span>{this.props.points}</span>
                    </div>
                    <i className="fas fa-arrow-down Joke-PointsButtons" onClick={this.handleDecPoints}></i>
                </div>
                <div className='Joke-Joke'>
                    {this.props.joke}
                </div>
                <div className='Joke-Rate'>
                    <i style={{fontSize: '3em', borderRadius: '50%'}} className={`Joke-Shadow ${this.props.rate}`}/>
                </div>
            </li>
        )
    }
}

export default Joke;