import React , { Component } from 'react';
import axios from 'axios';

class Jokes extends Component  {

    constructor(props){
        super(props);

        this.state = {
            jokes: [],
            loading: false
        }
        
        this.handleClickGetJokes = this.handleClickGetJokes.bind(this);
    }

    componentDidMount(){
        const jokes = this.getJokes();
        jokes.then((jokes) => {
            this.setState({jokes: jokes})
            console.log(this.state.jokes);
        })
    }

    handleClickGetJokes() {
        const jokes = this.getJokes();
        jokes.then((jokes) => {
            this.setState({jokes: jokes})
            console.log(this.state.jokes);
        })
    }

    getJokes(jokes = [...this.state.jokes], cont = 1){

        return axios
            .get("https://icanhazdadjoke.com/", {
            headers: { Accept: "application/json" }
        })
        .then(res => {

            const alreadyFound = jokes.some(joke => joke.id === res.data.id);
            const newJokes = alreadyFound
            ? jokes
            : jokes.concat([{ id: res.data.id, joke: res.data.joke }]);

            if (cont >= 10) {
                return newJokes;
            } else {
                return this.getJokes(newJokes, ++cont);
            }
        })
    }


    render(){

        let jokes;
        if(this.state.jokes.length > 0){
            jokes = this.state.jokes.map(joke => {
                return (<li key={joke.id}>{joke.joke}</li>);
            })
        }

        return(
            <div>
                <h1>Dad Joke</h1>
                <button onClick={this.handleClickGetJokes}>More Jokes</button>
                <ul style={{listStyle: 'none'}}>
                    {jokes}
                </ul>
            </div>
        )
    }
}

export default Jokes;
