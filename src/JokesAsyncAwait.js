import React , { Component } from 'react';
import Joke from './Joke';
import axios from 'axios';
import './Jokes.css';

const rates = [
    'far fa-sad-cry',
    'far fa-sad-tear',
    'far fa-meh-blank',
    'far fa-smile',
    'far fa-grin-alt',
    'far fa-laugh-wink',
    'far fa-grin-hearts'
];

class Jokes extends Component  {

    constructor(props){
        super(props);

        this.state = {
            jokes: [],
            loading: false
        }
        
        //this.handleGetJokes = this.handleGetJokes.bind(this);
        this.handlePoints = this.handlePoints.bind(this);
        this.getNewJokes = this.getNewJokes.bind(this);
    }

    componentDidMount(){
        this.handleGetJokes();
    }


    handleGetJokes(){
        let jokes = window.localStorage.getItem('jokes');
        if(jokes){
            this.setState({ jokes: JSON.parse(jokes), loading: false });
        }else{
            this.getNewJokes();
        }
    }
    

    getNewJokes() {
        
        let jokes = this.getJokes();
        
        jokes.then(jokes => {
            jokes = this.orderJokes(jokes);
            this.setState({jokes: jokes, loading: false})
            window.localStorage.setItem('jokes', JSON.stringify(jokes));
            //console.log(this.state.jokes);
        })
    }

    updateRate(joke){

        console.log(joke.points);
        
        if(joke.points <= -10){
            joke.rate = rates[0];
        }else if(joke.points > -10 && joke.points < 0){
            joke.rate = rates[1];
        }else if(joke.points === 0){
            joke.rate = rates[2];
        }else if(joke.points > 0 && joke.points <= 3){
            joke.rate = rates[3];
        }else if(joke.points > 3 && joke.points <= 6){
            joke.rate = rates[4];
        }else if(joke.points > 6 && joke.points <= 9){
            joke.rate = rates[5];
        }else if(joke.points > 9){
            joke.rate = rates[6];
        }

        return joke;
    }

    orderJokes(jokes){
        jokes.sort((a,b) => (a.points < b.points) ? 1 : ((b.points < a.points) ? -1 : 0)); 
        return jokes;
    }

    turnOffFocus(){
        const njokes = this.state.jokes.map(joke => {
            joke.focus = false;
            return {...joke};
        });

        console.log(njokes);
        return njokes;
    }

    handlePoints(op, id){

        const jokes = this.turnOffFocus();
    
        
        let newJoke = [...jokes.filter(joke => joke.id === id)];
        op === 'inc' ? newJoke[0].points = newJoke[0].points + 1 : newJoke[0].points = newJoke[0].points - 1;
        newJoke[0].focus = true;

        newJoke = this.updateRate(newJoke[0]);

        let index = jokes.map(joke => joke.id).indexOf(id);
        //let index2 = jokes.findIndex(joke => joke.id === id);
        let newJokes = [ ...jokes.slice(0, index) , newJoke , ...jokes.slice(index + 1, jokes.length) ];

        //sort jokes by the points
        newJokes = this.orderJokes(newJokes);
           
        this.setState({
            jokes: newJokes
        });

        window.localStorage.setItem('jokes', JSON.stringify(newJokes));
    }


    async getJokes() {

        this.setState({
            loading: true
        });

        const jokes = this.turnOffFocus();
        
        let cont = 1;

        while(cont <= 10){

            try{
                const res = await axios.get("https://icanhazdadjoke.com/", { headers: { Accept: "application/json" }});
    
                if(!jokes.some(joke => joke.id === res.data.id)){
                    jokes.push({ id: res.data.id, joke: res.data.joke, points: 0, rate: rates[2], focus: false })
                    cont++;
                }
            }catch(err){
                console.log('Error getting data from db.', err);
            }
        }
        
        return jokes;
    }


    render(){

        let loading = <div className='JokesSpinner'></div>
        let jokes;
        if(this.state.jokes.length > 0){
            jokes = this.state.jokes.map(joke => {
                return (<Joke key={joke.id} id={joke.id} joke={joke.joke} focus={joke.focus} points={joke.points} rate={joke.rate} handlePoints={this.handlePoints}/>);
            })
        }

        return(
            <div className='Jokes'>
                <h1 style={{color: "white", fontSize: '4.2em', fontWeight: '300'}}>Dad Jokes</h1>
                <button className='JokesButton' onClick={this.getNewJokes}>More Jokes</button>
                { this.state.loading ? loading : null }
                <ul style={{listStyle: 'none'}}>
                    {this.state.loading ? null : jokes}
                </ul>
            </div>
        )
    }
}

export default Jokes;
